![Pipline Status](https://gitlab.crja72.ru/django_2023/students/162521-mynamerandom-47231/badges/main/pipeline.svg)

## Установка
- Установите и активируйте виртуальное окружение
```
pip install venv
python -m venv venv
source venv/bin/activate
```

## Установка зависимости из файла requirements.txt
```
pip install -r requirements/prod.txt
```
- Для разработки
pip install -r requirements/dev.txt
- Для тестов
pip install -r requirements/test.txt

## Настройка переменных окружения
- Скопируйте файл `config.env` в `.env`
```
cp config.env .env
```

## Запуск
- В папке с файлом manage.py выполить команду:
```
python manage.py loadstatic
python manage.py runserver
```