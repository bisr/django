from datetime import timedelta
from unittest.mock import patch

from django.contrib.auth.models import User
from django.test import Client, override_settings, TestCase
from django.urls import reverse
from django.utils import timezone

__all__ = ["SignupTests"]


class SignupTests(TestCase):
    def test_signup_correct_context(self):
        response = Client().get(reverse("users:signup"))
        self.assertIn("form", response.context)

    def test_signup_with_valid_form(self):
        before_user_count = User.objects.count()
        form_data = {
            "username": "testname",
            "email": "testmail@ya.ru",
            "password1": "mypassfortest123",
            "password2": "mypassfortest123",
        }

        Client().post(
            reverse("users:signup"),
            data=form_data,
        )
        after_user_count = User.objects.count()
        self.assertEqual(before_user_count, after_user_count - 1)

    def test_signup_with_not_valid_form(self):
        before_user_count = User.objects.count()
        form_data = {
            "username": "testname",
            "email": "testmail@ya.ru",
            "password1": "mypassfortest123",
            "password2": "Mypassfortest123",
        }

        Client().post(
            reverse("users:signup"),
            data=form_data,
        )
        after_user_count = User.objects.count()
        self.assertEqual(before_user_count, after_user_count)

    @override_settings(DEFAULT_USER_IS_ACTIVE=False)
    def test_activate(self):
        form_data = {
            "username": "testname",
            "email": "testmail@ya.ru",
            "password1": "mypassfortest123",
            "password2": "mypassfortest123",
        }

        Client().post(
            reverse("users:signup"),
            data=form_data,
        )
        self.assertIs(
            User.objects.get(username=form_data["username"]).is_active,
            False,
        )
        Client().get(
            reverse("users:activate", args=(form_data["username"],)),
        )
        self.assertIs(
            User.objects.get(username=form_data["username"]).is_active,
            True,
        )

    @override_settings(DEFAULT_USER_IS_ACTIVE=False)
    def test_not_activate(self):
        form_data = {
            "username": "testname",
            "email": "testmail@ya.ru",
            "password1": "mypassfortest123",
            "password2": "mypassfortest123",
        }

        Client().post(
            reverse("users:signup"),
            data=form_data,
        )
        fake_time = timezone.now() + timedelta(hours=12, seconds=20)
        with patch("django.utils.timezone.now", return_value=fake_time):
            Client().get(
                reverse("users:activate", args=(form_data["username"],)),
            )
        self.assertIs(
            User.objects.get(username=form_data["username"]).is_active,
            False,
        )
