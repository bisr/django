import re

import django.conf

__all__ = ["ReformatRequestMiddleware"]

WORDS_REGEX = re.compile(r"\w+|\W+")
RUSSIAN_REGEX = re.compile(r"^[а-яА-ЯёЁ]+$")


class ReformatRequestMiddleware:
    response_count = 0

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        self.response_count += 1
        response = self.get_response(request)
        if self.response_count == 10 and django.conf.settings.ALLOW_REVERSE:
            text = response.content.decode()
            words = WORDS_REGEX.findall(text)

            new_text = [
                word[::-1] if RUSSIAN_REGEX.search(word) else word
                for word in words
            ]
            self.response_count = 0
            response.content = "".join(new_text).encode()
        return response
