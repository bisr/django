import django.core.exceptions
from django.test import Client, TestCase
from django.urls import reverse

import catalog.models

__all__ = ["ModelsTests", "StaticURLTests"]


class StaticURLTests(TestCase):
    def test_catalog_endpoint(self):
        response = Client().get(reverse("catalog:item_list"))
        self.assertEqual(response.status_code, 200)


class ModelsTests(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.category = catalog.models.Category.objects.create(
            is_published=True,
            name="Тестовая категория",
            slug="test-category-slug",
            weight=100,
        )
        cls.tag = catalog.models.Tag.objects.create(
            is_published=True,
            name="Тестовый тэг",
            slug="test-tag-slug",
        )

    def test_unable_item_text_create(self):
        item_count = catalog.models.Item.objects.count()
        with self.assertRaises(django.core.exceptions.ValidationError):
            self.item = catalog.models.Item(
                name="Тестовый товар",
                category=self.category,
                text="Нет нужного слова",
            )
            self.item.full_clean()
            self.item.save()
            self.item.tags.add(ModelsTests.tag)

        self.assertEqual(catalog.models.Item.objects.count(), item_count)

    def test_item_text_create(self):
        item_count = catalog.models.Item.objects.count()
        self.item = catalog.models.Item(
            name="Тестовый товар",
            category=self.category,
            text="Есть нужное слово: Превосходно",
        )
        self.item.full_clean()
        self.item.save()
        self.item.tags.add(ModelsTests.tag)

        self.assertEqual(catalog.models.Item.objects.count(), item_count + 1)


class ContextTests(TestCase):
    fixtures = ["fixtures/data.json"]

    def test_item_list_show_correct_context(self):
        response = Client().get(reverse("catalog:item_list"))
        self.assertIn("items", response.context)

    def test_item_list_items_size(self):
        response = Client().get(reverse("catalog:item_list"))
        items = response.context["items"]
        self.assertEqual(len(items), 2)

    def test_item_list_items_types(self):
        response = Client().get(reverse("catalog:item_list"))
        self.assertTrue(
            all(
                isinstance(
                    item,
                    catalog.models.Item,
                )
                for item in response.context["items"]
            ),
        )

    def test_item_detail_item_type(self):
        response = Client().get(reverse("catalog:item_detail", args=(21,)))
        self.assertIsInstance(response.context["item"], catalog.models.Item)


class DataBaseTests(TestCase):
    fixtures = ["fixtures/data.json"]

    def test_home_page_existing_fields(self):
        response = Client().get(reverse("homepage:home"))
        item = response.context["items"][0].__dict__
        self.assertIn("name", item)
        self.assertIn("tags", item["_prefetched_objects_cache"])
        self.assertNotIn("is_published", item)
        self.assertNotIn("gallery_images", item["_prefetched_objects_cache"])
