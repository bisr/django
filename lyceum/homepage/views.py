from django.http import HttpResponse, HttpResponseNotAllowed
from django.shortcuts import render

import catalog.models
from homepage.forms import EchoForm


__all__ = ["coffee_response", "home"]


def home(request):
    items = catalog.models.Item.objects.on_main()
    template = "homepage/main.html"
    context = {"items": items}
    return render(request, template, context)


def coffee_response(request):
    user = request.user
    if user.is_authenticated:
        user.profile.coffe_count += 1
        user.save()
    return HttpResponse("Я чайник", status=418)


def echo(request):
    if request.method == "GET":
        form = EchoForm()
        context = {"form": form}
        template = "homepage/echo.html"
        return render(request, template, context)
    return HttpResponseNotAllowed(["POST"])


def echo_submit(request):
    if request.method == "POST":
        form = EchoForm(request.POST or None)
        if form.is_valid():
            text = form.cleaned_data.get("text")
            return HttpResponse(text)
    return HttpResponseNotAllowed(["GET"])
