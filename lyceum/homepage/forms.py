from django import forms

__all__ = ["EchoForm"]


class EchoForm(forms.Form):
    text = forms.CharField(
        widget=forms.Textarea(attrs={"class": "form-control", "rows": 3}),
    )
