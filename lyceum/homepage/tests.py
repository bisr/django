from http import HTTPStatus

from django.test import Client, TestCase
from django.urls import reverse

import catalog.models

__all__ = ["StaticURLTests"]


class StaticURLTests(TestCase):
    def test_homepage_endpoint(self):
        response = Client().get(reverse("homepage:home"))
        self.assertEqual(response.status_code, 200)

    def test_homepage_coffee(self):
        response = Client().get(reverse("homepage:coffee"))
        self.assertEqual(response.status_code, HTTPStatus.IM_A_TEAPOT.value)
        self.assertEqual(response.content, "Я чайник".encode())


class ContextTests(TestCase):
    fixtures = ["fixtures/data.json"]

    def test_home_page_show_correct_context(self):
        response = Client().get(reverse("homepage:home"))
        self.assertIn("items", response.context)

    def test_home_page_items_size(self):
        response = Client().get(reverse("homepage:home"))
        items = response.context["items"]
        self.assertEqual(len(items), 2)

    def test_home_page_items_types(self):
        response = Client().get(reverse("homepage:home"))
        items = response.context["items"]
        self.assertTrue(
            all(isinstance(item, catalog.models.Item) for item in items),
        )


class DataBaseTests(TestCase):
    fixtures = ["fixtures/data.json"]

    def test_home_page_existing_fields(self):
        response = Client().get(reverse("homepage:home"))
        item = response.context["items"][0].__dict__
        self.assertIn("name", item)
        self.assertIn("tags", item["_prefetched_objects_cache"])
        self.assertNotIn("is_published", item)
        self.assertNotIn("gallery_images", item["_prefetched_objects_cache"])
