from django import forms

from feedback.models import Feedback

__all__ = ["FeedbackForm"]


class FeedbackForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.visible_fields():
            field.field.widget.attrs["class"] = "form-control"

    class Meta:
        model = Feedback
        exclude = ["created_on", "status"]
        labels = {
            "name": "Имя",
            "text": "Текст",
            "mail": "Почта",
        }
        help_texts = {
            "name": "Введите ваше имя",
            "text": "Введите текст",
            "mail": "Введите адресс вашей электронной почты",
        }
