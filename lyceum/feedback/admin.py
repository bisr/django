from django.contrib import admin

import feedback.models

__all__ = ["FeedbackAdmin"]


@admin.register(feedback.models.Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        fromm = self.model.objects.get(pk=obj.pk).status
        to = obj.status
        if fromm != to:
            feedback.models.StatusLog.objects.create(
                user=request.user,
                feedback=obj,
                fromm=fromm,
                to=to,
            )
        super().save_model(request, obj, form, change)
