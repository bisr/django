from django.conf import settings
from django.db import models

__all__ = ["Feedback"]


class Status(models.TextChoices):
    NEW = "получено", "получено"
    WIP = "в обработке", "в обработке"
    ANSWERED = "ответ дан", "ответ дан"


class Feedback(models.Model):
    name = models.CharField(max_length=150, blank=True, null=True)
    text = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    mail = models.EmailField()
    status = models.CharField(
        max_length=20,
        choices=Status.choices,
        default=Status.NEW,
    )


class StatusLog(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    timestamp = models.DateTimeField(auto_now_add=True)
    feedback = models.ForeignKey(Feedback, on_delete=models.CASCADE, null=True)
    fromm = models.CharField(max_length=20, null=True, db_column="from")
    to = models.CharField(max_length=20, null=True)
