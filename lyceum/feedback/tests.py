from django.test import Client, TestCase
from django.urls import reverse

from feedback.forms import FeedbackForm
from feedback.models import Feedback

__all__ = ["ContextTests"]


class ContextTests(TestCase):
    def test_feedback_correct_context(self):
        response = Client().get(reverse("feedback:feedback"))
        self.assertIn("form", response.context)

    def test_form_labels_and_help_texts(self):
        form = FeedbackForm()
        text_label = form.fields["text"].label
        self.assertEqual(text_label, "Текст")
        text_help_text = form.fields["text"].help_text
        self.assertEqual(text_help_text, "Введите текст")
        mail_label = form.fields["mail"].label
        self.assertEqual(mail_label, "Почта")
        mail_help_text = form.fields["mail"].help_text
        self.assertEqual(
            mail_help_text,
            "Введите адресс вашей электронной почты",
        )
        self.assertFormError

    def test_feedback_redirect(self):
        form_data = {
            "name": "Имя",
            "text": "Текст",
            "mail": "mail@mail.mail",
        }

        response = Client().post(
            reverse("feedback:feedback"),
            data=form_data,
            follow=True,
        )

        self.assertRedirects(response, reverse("feedback:feedback"))

    def test_form_errors(self):
        form_data = {
            "text": "Текст",
            "mail": "1111",
        }
        form = FeedbackForm(data=form_data)
        self.assertFormError(
            form,
            "mail",
            errors="Введите правильный адрес электронной почты.",
        )


class DataBaseTests(TestCase):
    def test_feedback_add_with_valid_form(self):
        before_feedback_count = Feedback.objects.count()
        form_data = {
            "name": "Имя",
            "text": "Текст",
            "mail": "mail@mail.mail",
        }

        Client().post(
            reverse("feedback:feedback"),
            data=form_data,
        )
        after_feedback_count = Feedback.objects.count()
        self.assertEqual(before_feedback_count, after_feedback_count - 1)

    def test_feedback_add_with_not_valid_form(self):
        before_feedback_count = Feedback.objects.count()
        form_data = {
            "name": "Имя",
            "text": "Текст",
            "mail": "123",
        }

        Client().post(
            reverse("feedback:feedback"),
            data=form_data,
        )
        after_feedback_count = Feedback.objects.count()
        self.assertEqual(before_feedback_count, after_feedback_count)
