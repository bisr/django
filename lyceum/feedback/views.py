from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.shortcuts import redirect, render

from feedback.forms import FeedbackForm

__all__ = ["feedback"]


def feedback(request):
    template = "feedback/feedback.html"
    form = FeedbackForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        send_mail(
            f'Привет {form.cleaned_data["name"]}',
            f'{form.cleaned_data["text"]}',
            settings.FEEDBACK_SENDER,
            [form.cleaned_data["mail"]],
            fail_silently=False,
        )
        form.save()

        messages.success(request, "Форма успешно отправлена")
        return redirect("feedback:feedback")
    context = {"form": form}
    return render(request, template, context)
